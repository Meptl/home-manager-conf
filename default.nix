{ config, pkgs, inputs, ... }@attrs:

with builtins;
{
  home.username = "yutoo";
  home.homeDirectory = "/home/yutoo";
  home.stateVersion = "23.11";
  programs.home-manager.enable = true;

  # I usually use this as a scratch space for os installed packages.
  home.packages = with pkgs; [
    nwg-look
    gnome3.nautilus
    helvum
    xorg.xrandr
  ];

  imports = [
    ./git
    ./dunst
  ];

  programs.neovim = (import ./vim pkgs );
  programs.alacritty = (import ./alacritty.nix pkgs);

  services.easyeffects = {
    enable = true;
    preset = "DenoiseCompressor";
  };

  gtk = {
    enable = true;
    font = {
      name = "DejaVu Sans";
      package = pkgs.dejavu_fonts;
    };
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome.adwaita-icon-theme;
    };
    theme = {
      name = "Adapta-Nokto-Eta";
      package = pkgs.adapta-gtk-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "gtk";
  };

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  home.file = {
      ".config/rofi/zsh_aliases.sh" = { text = "
        #!${pkgs.zsh}/bin/zsh -i
        alias | awk -F'[ =]' '{print $1}'
      "; };
      # ".config/hypr/hyprland.conf".source = ./hyprland.conf;
      ".config/rofi/config.rasi".source = ./confs/rofi/config.rasi;

      "bin/element-desktop".source = "/var/lib/flatpak/exports/bin/im.riot.Riot";
      "bin/webcord".source = "/var/lib/flatpak/exports/bin/io.github.spacingbat3.webcord";
      "bin/signal-desktop".source = "/var/lib/flatpak/exports/bin/org.signal.Signal";
      "bin/steam".source = "/var/lib/flatpak/exports/bin/com.valvesoftware.Steam";
      "bin/discord".source = "/var/lib/flatpak/exports/bin/com.discordapp.Discord";
  };
}
